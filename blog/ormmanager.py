from django.db import models

class GemManager(models.Manager):
    def get_queryset(self):
        print 'Haha ---------'
        superclass = super(GemManager, self)
        return superclass.get_queryset().filter(gemlike=True)
        
class AllAboutManager(models.Manager):
    '''Only return posts that are really good and all about X'''
    def all_about(self, text):
        posts = super(AllAboutManager, self).get_queryset().filter(
            gemlike=True,
            title__contains=text,
            content__contains=text)
        return posts