from __future__ import unicode_literals

from django.db import models
from django.contrib import admin

class BlogPost(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField()
    timestamp = models.DateTimeField()
    
    class Meta:
        ordering = ('-timestamp',)
    
class BlogPostAdmin(admin.ModelAdmin):
    list_display = ('title', 'timestamp')
    list_filter = ('title', 'timestamp')
    search_fields = ('title',)

admin.site.register(BlogPost, BlogPostAdmin)

class Person(models.Model):
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    
class PersonAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Name", {"fields": ("firstname", "lastname")}),
        ("Location", {"fields": ("city", "state")})
    ]
    
admin.site.register(Person, PersonAdmin)

class Post(models.Model):
    '''My example Post class'''
    from ormmanager import GemManager, AllAboutManager
    
    title = models.CharField(max_length=100)
    content = models.TextField()
    gemlike = models.BooleanField(default=False)
    
    objects = AllAboutManager()
    gems = GemManager()
    
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'gemlike')
    
admin.site.register(Post, PostAdmin)