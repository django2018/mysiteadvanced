from django.conf.urls import url
from blog.views import archive, vcard, csv_file, pie_chart, ormmanager, home_view, simple_template_view, mako
from blog.feeds import RSSFeed

urlpatterns = [
    url(r'^$', archive),
    url(r'^feeds/(?P<url>.*)/$', RSSFeed()),
    url(r'^vcard/(?P<person_pk>\d+)/$', vcard),
    url(r'^csv/$', csv_file),
    url(r'^chart/$', pie_chart),
    url(r'^ormmanager/$', ormmanager),
    
    url(r'^homeview/$', home_view),
    url(r'^simple/(?P<name>[a-z A-Z \d]+)/$', simple_template_view),
    url(r'^mako/$', mako),
]
