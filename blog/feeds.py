from django.contrib.syndication.views import Feed
from blog.models import BlogPost

class RSSFeed(Feed):
    title = "My awesome blog feed"
    description = "The lastest from my awesome blog"
    link = "/blog/"
    item_link = link
    
    def items(selfI):
        return BlogPost.objects.all()[:10]