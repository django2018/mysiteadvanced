from django.shortcuts import render

from django.template import loader, Context
from django.http import HttpResponse
from blog.models import BlogPost, Person

def archive(request):
    posts = BlogPost.objects.all()
    t = loader.get_template('archive.html')
    c = Context({ 'posts': posts })
    return HttpResponse(t.render(c))

def vcard(request, person_pk):
    import vobject
    person = Person.objects.get(pk=person_pk)
    print person
    v = vobject.vCard()
    v.add('n')
    v.n.value = vobject.vcard.Name(family=person.lastname, given=person.firstname)
    v.add('fn')
    v.fn.value = "%s %s" % (person.firstname, person.lastname)
    v.add('city')
    v.city.value = person.city
    output = v.serialize()
    filename = "%s%s.vcf" % (person.firstname, person.lastname)
    response = HttpResponse(output, content_type='text/x-vCard')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response
    
def csv_file(request):
    import csv
    response = HttpResponse(content_type='text/csv')
    response_writer = csv.writer(response)
    people = Person.objects.all().values_list('id', 'firstname', 'lastname', 'city', 'state')
    for p in people:
        response_writer.writerow(p)
    response['Content-Disposition'] = 'attachment; filename=everybody.csv'
    return response
    
def pie_chart(request):
    import sys, cairo, pycha.pie
    data = (
        ('Io on Eyedrops', 61),
        ('Haskell on Hovercrafts', 276),
        ('Lua on Linseed', 99),
        ('Django', 1000),
    )
    dataset = [(item[0], [[0, item[1]]]) for item in data]
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 750, 450)
    ticks = [dict(v=i, label=d[0]) for i, d in enumerate(data)]
    options = {
        'axis': {'x': {'ticks': ticks}},
        'legend': {'hide': True}
    }
    chart = pycha.pie.PieChart(surface, options)
    chart.addDataset(dataset)
    chart.render()
    response = HttpResponse(content_type='image/png')
    surface.write_to_png(response)
    response['Content-Disposition'] = 'attachment; filename=pie.png'
    return response
    
def ormmanager(request):
    from models import Post
    print '-----'
    really_goods_posts = Post.objects.filter(gemlike=True).values()
    print really_goods_posts
    print '*****'
    really_goods_posts_gems = Post.gems.all().values()
    print really_goods_posts_gems
    print '*****'
    cat_posts = Post.objects.filter(
        gemlike=True, 
        title__contains='cats',
        content__contains='cats').values()
    print cat_posts
    print '*****'
    cat_posts_manage = Post.objects.all_about('cats').values()
    print cat_posts_manage
    print '*****'
    response = HttpResponse('<h1>OK</h1>')
    return response
    
def home_view(request):
    import os
    import random
    import posixpath
    from django.shortcuts import render_to_response
    from django.conf import settings
    
    img_files = os.listdir(settings.RANDOM_IMG_DIR)
    print img_files
    img_name = random.choice(img_files)
    img_src = posixpath.join(settings.MEDIA_URL, 'img', img_name)
    # ... other view processing goes here ...
    return render_to_response('home.html', {'img_src':  img_src, 'content': 'ContentWikiFi ContentAb', 'my_string': 'Filters with an extra arguments'})
    
def simple_template_view(request, name):
    template = "Hello, your IP address is %s."
    return HttpResponse(template % request.META['REMOTE_ADDR'])
    
def mako(request):
    from mako.template import Template
    t = Template("Your IP address is ${REMOTE_ADDR}")
    output = t.render(**request.META)
    return HttpResponse(output)