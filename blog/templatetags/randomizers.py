import os
import random
import posixpath
from django import template
from django.conf import settings

register = template.Library()

def files(path, types=['.jpg', '.jpeg', '.png', '.gif']):
    fullpath = os.path.join(settings.MEDIA_ROOT, path)
    return [f for f in os.listdir(fullpath) if os.path.splitext(f)[1] in types]
    
@register.simple_tag
def random_image(path):
    pick = random.choice(files(path))
    return posixpath.join(settings.MEDIA_URL, path, pick)
    
@register.simple_tag
def random_file(path, ext):
    pick = random.choice(files(path, ['.' + ext]))
    return posixpath.join(settings.MEDIA_URL, path, pick)