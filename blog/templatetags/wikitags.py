import re
from django.template import Library
from django.conf import settings

register = Library()
wikifier = re.compile(r'\b(([A-Z]+[a-z]+){2,})\b')

@register.filter
def wikify(text):
    return wikifier.sub(r'<a href="/wiki/\1/">\1</a>', text)
    
@register.filter
def grep(text, term):
    '''Displays a string only if that string contains a certain sequence of characters'''
    if term in text:
        return text
        
@register.filter
def hide_if_shorter_than(text, min_len):
    if len(text) >= int(min_len):
        return text